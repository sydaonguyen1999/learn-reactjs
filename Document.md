# Component life cycles

### Với component của reactjs  life cycles gồm có 4 giai đoạn chính
  #### Khởi tạo (Initialization): 

   - Đây là giai đoạn mà component bắt đầu hành trình của mình bằng cách khởi tạo state và props và điều này thường được thực hiện bên trong phương thức constructor.
  #### Được tạo ra (Mounting): 

   - Giai đoạn này được thực hiện sau khi Initialization được hoàn thành. Đây là quá trình chuyển Dom ảo thành DOM và hiển thị trên trình duyệt, và component sẽ được render lần đầu. Ở đây chúng ta sẽ có 3 phương thức tham gia.
     ##### componentWillMount():
     - Được chạy khi một component chuẩn bị mount tức là trước khi thực hiện render. Sau khi thực hiện xong componentWillMount() thì component mới có thể được mount.
        => Lưu ý: Chúng ta không nên thực hiện thay đổi liên quan đến state, props hay call API ở đây. Bởi vì tại hàm này thời gian chuẩn bị mount -> mount rất ngắn nên sẽ ảnh hưởng đến kết quả render không như ý muốn.
     ##### componentDidMount():
     - Được gọi khi component đã được mount (render thành công ), quá trình này xảy ra sau khi componentWillMount() thực hiện xong. Trong phương thức này bạn có thể gọi API, thay đổi state, props.

  #### Qua nhiều thay đổi (Updating): 
   - Đây là giai đoạn thứ 3 mà component phải thực hiện sau 2 giai đoạn đầu (Initialization và Mounting) ở giai đoạn này sẽ thực hiện việc re-render lại component thông qua những sự kiện từ người dùng như click, nhập, gõ . . . ở giai đoạn này chúng ta có 4 phương thức chính.
      
      ##### shouldComponentUpdate(): 
      - Ở phương thức này nó sẽ nhận về 2 tham số truyền vào là nextState và nextProps.
      - Phương thức này xác định xem component có nên được render lại hay không ? Theo mặc định, nó trả về true. Nhưng bạn có thể thay đổi giá trị trả về của nó theo từng trường hợp.

      ##### componentWillUpdate():
      - Giống như shouldComponentUpdate(), componentWillUpdate() sẽ nhận vào 2 tham số đó là nextState và nextProps.
      - Phương thức này được gọi trước khi tiến hành re-render, bạn có thể thực hiện các hành động như update state, props,...trong phương thức này trước khi tiến hành re-render.
      ##### ComponentDidUpdate():
      - Phương thức này được gọi khi component đã re-render xong. Chúng ta có ví dụ về cả 3 phương thức về đề cập ở trên.

  #### Và bị bỏ đi (Unmounting): 
   - Ngược lại với Mounting, Unmounting là quá trình loại bỏ khỏi DOM.
      ##### componentWillUnmount():
      - Phương thức cuối cùng để kết thúc một vòng đời của component.