import './App.css';
import 'antd/dist/antd.css';
// import Demo from './components/demo'
import UserComponent from './components/TestAPI/UserComponent';
function App() {
  return (
    <div className='container'>
      {/* <Demo></Demo> */}
      <UserComponent></UserComponent>
    </div>
  );
}

export default App;
