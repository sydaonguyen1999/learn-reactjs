import 'antd/dist/antd.css'
import { Component } from 'react';
import axios from 'axios';
import { Button } from 'antd';



export default class UserComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserList: []
    }
  };
  componentDidMount(){
    this.getList()
  }
  getList = () => {
    axios.get(`https://jsonplaceholder.typicode.com/users`)
    .then(res => {
      let data = res.data
      this.setState({
        UserList: data
      })
      console.log('get UserList:', this.state.UserList)
    })
    .catch(error => console.error(error))
  }

  render() {
    return (
      <>
        <div className='container'>
          <p> UserComponent</p>
        </div>
      </>
    );
  }
}

