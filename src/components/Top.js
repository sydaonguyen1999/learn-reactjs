import { Component } from 'react';

class Top extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: 0,
      result2: 0,
      result3: 0,
      result4: 0,

      case1_a: 0,
      case2_a: 0,
      case3_a: 0,
      case4_a: 0,

      case1_b: 0,
      case2_b: 0,
      case3_b: 0,
      case4_b: 0,
    };
  }

  formatCurrency = (number) => {
    number = number.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
    return number
  }

  
  changeNum_a = (a, change_case) => {
      if(change_case === 'case-1'){
       this.setState({ case1_a: Number(a) })
    }
    
      if(change_case === 'case-2'){
        this.setState({ case2_a: Number(a) })
    }

      if(change_case === 'case-3'){
        this.setState({ case3_a: Number(a) })
    }

      if(change_case === 'case-4'){
      this.setState({ case4_a: Number(a) })
    }
  }

  changeNums_b = (b, change_case) => {
    if(change_case === 'case-1'){
       this.setState({ case1_b: Number(b) }, () => {
          if(this.state.case1_a){
            let result_temp = this.state.case1_b * this.state.case1_a / 100
            result_temp = result_temp.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
            this.setState({ result: result_temp })
        }
       })
    }

    if(change_case === 'case-2'){
      this.setState({ case2_b: Number(b) }, () => {
         if(this.state.case2_a){
           let result_temp = this.state.case2_a / this.state.case2_b *  100
          //  result_temp = result_temp.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
           this.setState({ result2: result_temp })
       }
      })
   }


  }


  onClick = ()=> {
    console.log('log on onClick Function', this.state)
  }


  render(){
    return(
      <div className='container shadow p-3 mb-5 bg-white rounded'>
        <div>
          <b>Tính X% của a:</b>
          <div className='row'>
              <div className='col-4 row'>
                <div className='col-9'>
                  <input type='number' onChange={a => this.changeNum_a(a.target.value, 'case-1')} placeholder='nhập a'></input>
                </div>
                <div className='col-3'>
                 a% của
                </div>
              </div>

              <div className='col-3'>
                  <input type='number' onChange={b => this.changeNums_b(b.target.value, 'case-1')} placeholder='nhập b'></input>
              </div>

              <div className='col-5 row'>
                <div className='col-1'>
                  =
                </div>

                <div className='col-2'>
                  <button type="submit" className="btn btn-primary">
                  { this.state.result }
                  </button>
                </div>
              </div>
          </div>
        </div>

        <div>
          <b>Tính xem X là bao nhiêu % của A:</b>
          <div className='row'>
              <div className='col-4 row'>
                <div className='col-7'>
                  <input type='number' placeholder='X' onChange={a => this.changeNum_a(a.target.value, 'case-2')}></input>
                </div>
                <div className='col-5'>
                là bao nhiêu % của
                </div>
              </div>

              <div className='col-3'>
                  <input type='number' placeholder='A' onChange={b => this.changeNums_b(b.target.value, 'case-2')}></input>
              </div>

              <div className='col-5 row'>
                <div className='col-1'>
                  =
                </div>

                <div className='col-2'>
                  <button type="submit" className="btn btn-primary">
                    { this.state.result2 }%
                  </button>
                </div>
              </div>
          </div>
        </div>

        <div>
          <b>Tính phần trăm của giá trị thay đổi:</b>
          <div className='row'>
              <div className='col-4 row'>
                <div className='col-5'>
                   Tỷ lệ tăng / giảm
                </div>
                <div className='col-6'>
                  <input type='number' placeholder='nhập a'></input>
                </div>
                <div className='col-1'>-</div>
              </div>

              <div className='col-3'>
                  <input type='number' placeholder='nhập b'></input>
              </div>

              <div className='col-5 row'>
                <div className='col-1'>
                  =
                </div>
                <div className='col-2'>
                  <button type="submit" className="btn btn-primary">
                      Kết quả
                  </button>
                </div>
              </div>
          </div>
        </div>  

        <div>
          <b>Để tăng hoặc giảm một tỷ lệ cụ thể:</b>
          <div className='row'>
              <div className='col-4 row'>
                <div className='col-9'>
                  <input type='number' placeholder='nhập a'></input>
                </div>
                <div className='col-3'>
                 a% của
                </div>
              </div>

              <div className='col-3'>
                  <input type='number' placeholder='nhập b'></input>
              </div>

              <div className='col-5 row'>
                <div className='col-1'>
                  =
                </div>
                <div className='col-5'>
                  <input type='number' placeholder='nhập c'></input>
                </div>

                <div className='col-2'>
                  <button type="submit" className="btn btn-primary">
                      Submit
                  </button>
                </div>
              </div>
          </div>
        </div>

      </div>
    )
  }
}

export default Top;
