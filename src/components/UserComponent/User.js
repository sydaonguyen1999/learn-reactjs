import 'antd/dist/antd.css'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { Button, Table, Form, Input, DatePicker, Modal } from 'antd'
import { Component } from 'react';


export default class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tempArr: [
        {
          id: '',
          key: '',
          username: '',
          birthday: '',
          address: '',
          phone: '',
        }
      ],

      dataSource: [],

      columns: [
        {
          title: 'ID',
          dataIndex: 'id',
          key: 'id',
        },
        {
          title: 'Name',
          dataIndex: 'username',
          key: 'username',
        },
        {
          title: 'Birthday',
          dataIndex: 'birthday',
          key: 'birthday',
        },
        {
          title: 'Address',
          dataIndex: 'address',
          key: 'address',
        },
        {
          title: 'Phone',
          dataIndex: 'phone',
          key: 'phone',
        },
        {
          title: 'Action',
          key: 'Action',
          render: (index) => {
            return <>
              <EditOutlined style={{ color: "blue" }} />

              <DeleteOutlined style={{ color: "red", marginLeft: 10 }} />
            </>
          }
        },
      ],

    }
    this.changeValue = this.changeValue.bind(this)
    this.addNew = this.addNew.bind(this)
  };

  addNew = (event) => {
    event.preventDefault()
    let checkID = false
    checkID = this.state.dataSource.some((item) => { return item.id && Number(item.id) === this.state.tempArr.id })
    if (checkID) {
      Modal.error({ title: 'Information already exists in the system', content: 'Please try again' });
      this.reset()
    }
    if (!checkID) {
      this.state.tempArr.id
        ? this.setState({ dataSource: [...this.state.dataSource, this.state.tempArr] })
        : Modal.error({ title: 'No information', content: 'Please complete all information' });
    }
  }

  reset = () => {
    this.setState({
      tempArr: {},
      address: '',
      code: '',
      username: '',
      email: '',
      birthday: '',
      phone: '',
    })
  }

  ranNum = () => {
    var numberRan = parseInt(Math.random() * 1000)
    return numberRan
  }

  changeDatePicker = (date, dateString) => {
    this.setState({
      birthday: dateString
    })
  }


  changeValue = (e) => {
    var target = e.target
    var name = target.name
    var value = target.value
    let ID = this.ranNum()
    let KEY = ID
    let UserTemp = {
      id: ID,
      code: this.state.code,
      key: KEY,
      username: this.state.username,
      birthday: this.state.birthday,
      address: this.state.address,
      phone: this.state.phone
    }
    this.setState({
      [name]: value,
      tempArr: UserTemp,
    })
  }



  render() {
    return (
      <div className='container'>
        <Form
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 14 }}
          layout="horizontal">

          <Form.Item label="code">
            <Input name='code' value={this.state.code} onChange={this.changeValue} />
          </Form.Item>

          <Form.Item label="Name">
            <Input name='username' value={this.state.username} onChange={this.changeValue} />
          </Form.Item>

          <Form.Item label="Birthday">
            <DatePicker name='birthday' onChange={this.changeDatePicker} />
          </Form.Item>

          <Form.Item label="Address">
            <Input name='address' value={this.state.address} onChange={this.changeValue} />
          </Form.Item>

          <Form.Item label="Phone">
            <Input name='phone' value={this.state.phone} onChange={this.changeValue} />
          </Form.Item>

          <Form.Item label="Email">
            <Input name='email' value={this.state.email} onChange={this.changeValue} />
          </Form.Item>

          <Form.Item>
            <Button onClick={this.reset} style={{ color: "red", float: 'right', marginLeft: 10 }}>Reset</Button>
            <Button onClick={this.addNew} style={{ color: "blue", float: 'right' }}>Add new User</Button>

          </Form.Item>

        </Form>
        <Table className='container' dataSource={this.state.dataSource} columns={this.state.columns}></Table>
      </div>
    );
  }

}